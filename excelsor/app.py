import logging
import os
import pathlib

import coloredlogs
from openpyxl import load_workbook
from sanic import Sanic
from sanic.request import Request
from sanic.response import json as r_json
from sanic_cors import CORS

from utils import DateTimeEncoder

logger = logging.getLogger(__name__)
coloredlogs.install(logging.DEBUG)

XCEL_FILE_PATH = pathlib.Path("files")
os.makedirs(str(XCEL_FILE_PATH), exist_ok=True)

app = Sanic(__name__)
CORS(app, automatic_options=True)


@app.route("upload", ["POST"])
def upload(request: Request):
    xcel_file = request.files.get("file")

    if xcel_file and xcel_file.type.startswith(
        "application/vnd.openxmlformats-officedocument"
    ):
        with open(XCEL_FILE_PATH / xcel_file.name, "wb") as f:
            f.write(xcel_file.body)
    else:
        return r_json({"error": "Invalid file."}, 400)
    return r_json({"name": xcel_file.name}, 200)


@app.route("xcel/<name:string>", ["GET"])
def get_xcel_data(request: Request, name: str):
    logger.debug(("filename", name))
    try:
        with open((XCEL_FILE_PATH / name), "rb") as f:
            wb = load_workbook(XCEL_FILE_PATH / name)
    except FileNotFoundError:
        return r_json({"error": f"{name} not found."}, 404)

    sheets = wb.sheetnames
    sheet = wb[sheets[0]]

    cell_values = []

    for row in sheet.iter_rows():
        row_values = []
        for cell in row:
            row_values.append({"value": cell.value})
        cell_values.append(row_values)

    for merged_cell in sheet.merged_cells.ranges:
        # excel uses 1-index so all index values should be converted to 0-index
        # when using python data types

        # all merged cell should be hidden
        for row in range(merged_cell.min_row, merged_cell.max_row + 1):
            for col in range(merged_cell.min_col, merged_cell.max_col + 1):
                cell_values[row - 1][col - 1]["style"] = {"display": "none"}

        row, col = merged_cell.left[0]
        colspan, rowspan = merged_cell.size["columns"], merged_cell.size["rows"]

        cell_values[row - 1][col - 1]["table"] = {
            "colSpan": colspan,
            "rowSpan": rowspan,
        }
        # only the starting cell should be displayed
        cell_values[row - 1][col - 1].pop("style")

    return r_json({"data": cell_values}, dumps=DateTimeEncoder().encode)


if __name__ == "__main__":
    app.run(port=8000, debug=True)
